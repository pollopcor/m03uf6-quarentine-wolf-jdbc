package programa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Scanner;

public class principal {

	public static Scanner scn = new Scanner(System.in);
	public static Connection con;

	public static void main(String[] args) {

		String url = "jdbc:mysql://localhost:3306/quarantine_wolf?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";

		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "super3");

		try {

			con = DriverManager.getConnection(url, propietatsConnexio);
			System.out.println("Base de dades connectada!");

		} catch (SQLException e) {
			System.err.println("Error d'apertura de connexió: " + e.getMessage());
		}

		
		int opcio;

		do {

			System.out.println(" 1) Veure Taules i Columnes");
			System.out.println(" 2) Veure Dades");
			System.out.println(" 3) Veure Partida");
			System.out.println(" 4) Veure Jugador");
			System.out.println(" 5) Actualitzar Partida");
			System.out.println(" 6) Escriure Missatge");
			System.out.println(" 7) Veure Missatges");
			System.out.println(" 8) ????");
			System.out.println(" 0) Sortir");
			
			do {

				try {
					opcio = scn.nextInt();
				} catch (InputMismatchException e) {
					opcio = -1;
				}

			} while (opcio < 0 || opcio > 8);

			System.out.println();
			scn.nextLine();
			
			// Escollir opcio
			switch (opcio) {
				case 1:
					veureTaulesColumnes();
					break;
	
				case 2:
					veureDades();
					break;
					
				case 3:
					veurePartida();
					break;
				
				case 4:
					veureJugador();
					break;
	
				case 5:
					actualitzarPartida();
					break;
					
				case 6:
					escriureMissatge();
					break;
					
				case 7:
					veureMissatges();
					break;
					
				case 8:
	
					break;
			}

			if(opcio != 0) {
				System.out.println();
				System.out.println("Enter per continuar...");
				scn.nextLine();
			}
			
		} while (opcio != 0);

		// Tancar conexio a la BD
		if (con != null)
			try {
				con.close();
			} catch (SQLException e) {
				System.err.println("Error de tancament de connexió: " + e.getMessage());
			}

	}

	
	public static String espaciar(String texto, int espacios) {
		return String.format("%-" + espacios + "s", texto);
	}
	
	//
	// 1 - Veure taules i columnes
	//
	public static void veureTaulesColumnes() {
		ResultSet res;
		
		try {

			// Agafar dades de les taules (nom de les taules)
			res = con.createStatement().executeQuery("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'quarantine_wolf'");

			while (res.next()) {
				
				String nomTaula = res.getString(1);
				System.out.println("[Taula] Nom:" + nomTaula);
				
				// Agafar dades de les columns d'aquesta taula
				ResultSet resCols = con.createStatement().executeQuery("SELECT column_name, is_nullable, column_type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + nomTaula + "'");
				
				while(resCols.next()) {
					
					String nomCol = resCols.getString(1);
					String nullableCol = resCols.getString(2);
					String tipusCol = resCols.getString(3);
					
					System.out.println("   [Columna] Nom: " + String.format("%-" + 24 + "s", nomCol) + "   Is_nullable: " + String.format("%-" + 4 + "s", nullableCol ) + "   Tipus:" + tipusCol);	
				}
				System.out.println();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	//
	// 2 - Veure dades
	//
	public static void veureDades() {
		
		ResultSet res;
		
		System.out.println("Digues el nom de la taula:");
		String veureTaula = scn.nextLine();
		
		if(veureTaula.isEmpty())
			return;
		
		try {

			// Agafar les dades de la taula
			try {
				res = con.createStatement().executeQuery("SELECT * FROM " + veureTaula);
			} catch (SQLSyntaxErrorException e) {
				System.err.println("No existeix la taula \"" + veureTaula + "\""); 
				return;
			}

			// Agafar meta-dades de la consulta
			ResultSetMetaData resMeta = res.getMetaData();
			
			while (res.next()) {
			
				for(int i=1; i<= resMeta.getColumnCount();i++) {
					System.out.print(resMeta.getColumnName(i) + ": " + String.format("%-" + 10 + "s", res.getString(i)) + " ");
				}
				System.out.println();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	//
	// 3 - Veure partida
	//
	public static void veurePartida() {
		
		System.out.println("Digues un numero de partida:");
		int numPart;
		ResultSet res;
		
		do {
			try {
				numPart = scn.nextInt();
			} catch(InputMismatchException e) {
				System.err.println("Numero invalid");
				numPart = -1;
			}
		} while(numPart < 0);
		
		try {

			// Agafar partida on la ID sigui la especificada
			res = con.createStatement().executeQuery("SELECT game_state FROM quarantine_wolf.games WHERE game_id = " + numPart);
			
			// Intenta moure el cursor, si falla esta buit
			if(!res.next()) {
				System.err.println("No existeix partida amb ID " + numPart);
				return;
			}
			
			ResultSet res1;
			
			// Oberta
			if(res.getString("game_state").equals("O")) {
				
				
				
			} else {
				
				// Tancada o Error
				res1 = con.createStatement().executeQuery(""
						+ "SELECT games.game_id, games.game_state, turn_death, users.user_id, user_firstname, user_lastname, rol_id\r\n" + 
						"FROM games\r\n" + 
						"JOIN games_users_rol ON games_users_rol.game_id = games.game_id\r\n" + 
						"JOIN users ON games_users_rol.game_id = games.game_id AND games_users_rol.user_id = users.user_id\r\n" + 
						"JOIN games_users_register ON games_users_register.game_id = games.game_id AND games_users_register.user_id = users.user_id\r\n" + 
						"WHERE games.game_id = " + numPart + " " +
						"ORDER BY turn_death");
				
				// Agafar meta-dades de la consulta
				ResultSetMetaData resMeta = res1.getMetaData();
				
				// Mostrar capçelera
				String sep = "";
				for(int i=1; i<= resMeta.getColumnCount();i++) {
					System.out.print(espaciar(resMeta.getColumnName(i), 15) + "|");
					sep += "---------------|";
				}
				System.out.println("\n" + sep);
				
				// Mostrar dades
				while(res1.next()) {
					for(int i=1; i<= resMeta.getColumnCount();i++)
						System.out.print(espaciar( (res1.getString(i) == null ? "" : res1.getString(i) ), 15) + "|" );

					System.out.println();
				}
				
				// Si Tancada, mostrar guanyador
				if(res.getString("game_state").equals("T")) {
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//
	// 4 - Veure jugador
	//
	public static void veureJugador() {
		ResultSet res;
		
		try {
			
			res = con.createStatement().executeQuery("SELECT user_login FROM quarantine_wolf.users");
			
			System.out.println("Usuaris:");
			while (res.next())
				System.out.println("- " + res.getString(1));
			
			System.out.println("\nDigues un usuari:");
			String login = scn.nextLine();
			
			ResultSet res2 = con.createStatement().executeQuery("SELECT * FROM quarantine_wolf.users WHERE user_login = '" + login + "'");
			ResultSetMetaData resMeta = res2.getMetaData();
			
			if(res2.next()) {
				
				// Mostrar noms dels camps
				for(int i=1; i<= resMeta.getColumnCount();i++)
					if(i != 5)
						System.out.print(espaciar(resMeta.getColumnName(i), 20));

				System.out.println();
				
				// Mostrar els valors dels camps
				for(int i=1; i<= resMeta.getColumnCount();i++)
					if(i != 5)
						System.out.print(espaciar(res2.getString(i), 20));
				System.out.println();
				
			} else
				System.out.println("L'usuari introduit no existeix a la taula");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	//
	// 5 - Actualitzar partida
	//
	public static void actualitzarPartida() {
		
		System.out.println("Digues un numero de partida:");
		int idPartida;
		ResultSet res;
		
		do {
			try {
				idPartida = scn.nextInt();
			} catch(InputMismatchException e) {
				System.err.println("Numero invalid");
				idPartida = -1;
			}
		} while(idPartida < 0);
		
		try {

			// Agafar partida on la ID sigui la especificada
			res = con.createStatement().executeQuery("SELECT game_state, updated FROM quarantine_wolf.games WHERE game_id = " + idPartida);

			// Intenta moure el cursor, si falla esta buit
			if(!res.next()) {
				System.err.println("No existeix cap partida amb ID " + idPartida);
				return;
			}
			
			// Mirar si esta tancada
			if(!res.getString("game_state").equals("C")) {
				System.out.println("No es pot actualitzar una partida que no estigui correctament tancada");
				return;
			}
			
			// Si esta actualitzada
			if(res.getBoolean("updated")) {
			
				System.out.println("La partida ja esta actualitzada");
			
			} else {
				
				// Actualitzar partida
				
				// Agafar user_id i turn_death dels usuaris d'aquella partida
				ResultSet resUsuaris = con.createStatement().executeQuery(""
						+ "SELECT users.user_id, turn_death FROM quarantine_wolf.games_users_register "
						+ "JOIN users ON games_users_register.user_id = users.user_id AND games_users_register.game_id = " + idPartida);
				
				// Per cada usuari de la partida, veure si ha mort o no
				while(resUsuaris.next()) {
					
					int userId = resUsuaris.getInt("user_id");
					
					// Si no ha mort
					if(resUsuaris.getString("turn_death") == null)
						con.createStatement().executeUpdate("UPDATE users SET user_won_games = user_won_games + 1 WHERE user_id = " + userId);
					else
						con.createStatement().executeUpdate("UPDATE users SET user_lost_games = user_lost_games + 1 WHERE user_id = " + userId);
				}
				
				// Marcar com actualitzada
				con.createStatement().executeUpdate("UPDATE quarantine_wolf.games SET updated = 1 WHERE game_id = " + idPartida);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//
	// 6 - Escriure missatge
	//
	public static void escriureMissatge() {
		
		ResultSet res;
		
		try {
			
			System.out.println("\nDigues l'usuari emissor del missatge:");
			String emissor = scn.nextLine();
			System.out.println("\nDigues l'usuari receptor del missatge:");
			String receptor = scn.nextLine();

			// Comprovar si son iguals
			if(emissor.equals(receptor)) {
				System.out.println("L'usuari emissor no pot ser igual a l'usuari receptor");
				return;
			}
			
			// Comprovar que existeixen i agafar IDs
			int emissorId, receptorId;
			
			res = con.createStatement().executeQuery("SELECT user_id FROM quarantine_wolf.users WHERE user_login = '" + emissor + "'");
			emissorId = (res.next() ? res.getInt(1) : -1);
			
			res = con.createStatement().executeQuery("SELECT user_id FROM quarantine_wolf.users WHERE user_login = '" + receptor + "'");
			receptorId = (res.next() ? res.getInt(1) : -1);

			if(emissorId == -1 || receptorId == -1) {
				System.out.println("No s'ha trobat l'usuari \"" + (emissorId == -1 ? emissor : receptor) + "\"");
				return;
			}
			
			System.out.println("Escriu el missatge:");
			String msg = scn.nextLine();
			
			// Crear missatge
			con.createStatement().executeUpdate("INSERT INTO messages (message_user_sender, message_user_receiver, message_content, message_date) VALUES (" + emissorId + ", " + receptorId + ", '" + msg + "', CURRENT_TIMESTAMP)");
			
			System.out.println("S'ha inserit el missatge a la BD");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	//
	// 7 - Veure missatges
	//
	public static void veureMissatges() {
		
		ResultSet res;
		
		System.out.println("Login de l'emissor");
		String nomEmissor = scn.nextLine().trim();
		System.out.println("Login del receptor");
		String nomReceptor = scn.nextLine().trim();
		
		// Mostrar missatge
		try {
		
			res = con.createStatement().executeQuery("SELECT message_id, emiss.user_login emissor, recep.user_login as receptor, message_content, message_date FROM messages " + 
					"JOIN users emiss ON messages.message_user_sender = emiss.user_id AND emiss.user_login LIKE '" + (!nomEmissor.isEmpty() ? nomEmissor : "%") + "'" + 
					"JOIN users recep ON messages.message_user_receiver = recep.user_id AND recep.user_login LIKE '" + (!nomReceptor.isEmpty() ? nomReceptor : "%") + "'");

			
			// Agafar meta-dades de la consulta
			ResultSetMetaData resMeta = res.getMetaData();
			
			// Assignar amplada de cada columna
			int [] amplada = new int[] {15, 15, 15, 40, 20};
			
			String valor;
			
			// Mostrar nom camps
			String cap = "";
			for(int i=1; i<= resMeta.getColumnCount();i++) {
				
				// Retallar el text si supera l'amplada de la columna (assignat desde l'array "espais")
				valor = resMeta.getColumnName(i);
				valor = (valor.length() > amplada[i-1] ? valor.substring(0, amplada[i-1] - 2) + ".." : valor);
				
				System.out.print(espaciar(valor, amplada[i-1]) + "|");
				
				// Allargar separador
				for(int x = 0;x<amplada[i-1];x++)
					cap += "-";
				cap+="|";
			}
			System.out.println("\n" + cap);			
			
			// Mostrar valors dels camps
			while(res.next()) {
				
				for(int i=1; i<= resMeta.getColumnCount();i++) {
					
					// Retallar el text si supera l'amplada de la columna (assignat desde l'array "espais")
					valor = (res.getString(i) != null ? res.getString(i) : "");
					valor = (valor.length() > amplada[i-1] ? valor.substring(0, amplada[i-1] - 2) + ".." : valor);
					
					System.out.print(espaciar(valor, amplada[i-1]) + "|");
				}
				System.out.println();
			}
			
						
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
}
